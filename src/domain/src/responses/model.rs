#[derive(Clone)]
pub struct Response {
    pub user_id: String,
    pub question_id: String,
    pub answered_index: i64,
}
