#[derive(Clone)]
pub struct Question {
    pub id: String,
    pub question: String,
    pub answers: Vec<String>,
}
