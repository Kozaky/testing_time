use crate::error::DomainError;
use async_trait::async_trait;

#[async_trait]
pub trait UserRepository {
    async fn count_users(
        &self,
        question_ids: &Vec<i64>,
        answer_ids: &Vec<i64>,
    ) -> Result<i64, DomainError>;
}
