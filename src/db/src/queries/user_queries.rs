use crate::connection::DBPool;
use crate::errors::DBError;

pub async fn count_users(
    db_pool: &DBPool,
    question_ids: &Vec<i64>,
    answer_ids: &Vec<i64>,
) -> Result<i64, DBError> {
    let con = db_pool.get().await?;
    let row = con
        .query_one(
            "SELECT COUNT(1) FROM (SELECT COUNT(1) FROM users u LEFT JOIN responses r on r.user_id = u.id WHERE r.question_id = ANY($1) AND r.answer_id = ANY($2) GROUP BY u.id) res WHERE res.count >= $3",
            &[question_ids, answer_ids, &(question_ids.len() as i64)],
        )
        .await?;

    Ok(row.get(0))
}
