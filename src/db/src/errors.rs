use domain::error::DomainError;
use mobc;
use tokio_postgres::Error;

#[derive(Debug)]
pub struct DBError {
    pub error_type: DBErrorType,
    pub cause: Option<String>,
}

#[derive(Debug)]
pub enum DBErrorType {
    Connection,
    NotFound,
    ForeignKeyViolation,
    VarcharSize,
    PostgreSQL,
}

impl From<mobc::Error<Error>> for DBError {
    fn from(error: mobc::Error<Error>) -> Self {
        Self {
            error_type: DBErrorType::Connection,
            cause: Some(error.to_string()),
        }
    }
}

impl From<Error> for DBError {
    fn from(error: Error) -> Self {
        match error.code() {
            Some(code) => match code.code() {
                "23503" => Self {
                    error_type: DBErrorType::ForeignKeyViolation,
                    cause: Some(error.to_string()),
                },
                "22001" => Self {
                    error_type: DBErrorType::VarcharSize,
                    cause: Some(error.to_string()),
                },
                _ => Self {
                    error_type: DBErrorType::PostgreSQL,
                    cause: Some(error.to_string()),
                },
            },
            None => match error.to_string().as_str() {
                msg @ "query returned an unexpected number of rows" => Self {
                    error_type: DBErrorType::NotFound,
                    cause: Some(msg.to_owned()),
                },
                msg => Self {
                    error_type: DBErrorType::PostgreSQL,
                    cause: Some(msg.to_owned()),
                },
            },
        }
    }
}

impl From<DBError> for DomainError {
    fn from(error: DBError) -> Self {
        DomainError { cause: error.cause }
    }
}
