use crate::connection::Connection;
use crate::queries::user_queries;
use async_trait::async_trait;
use domain::error::DomainError;
use std::sync::Arc;

pub struct UserRepository {
    pub con: Arc<Connection>,
}

impl UserRepository {
    pub fn new(con: Arc<Connection>) -> Self {
        Self { con }
    }
}

#[async_trait]
impl domain::UserRepository for UserRepository {
    async fn count_users(
        &self,
        question_ids: &Vec<i64>,
        answer_ids: &Vec<i64>,
    ) -> Result<i64, DomainError> {
        let total =
            user_queries::count_users(&self.con.db_pool, question_ids, answer_ids).await?;

        Ok(total)
    }
}
