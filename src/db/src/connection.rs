use mobc::Pool;
use mobc_postgres::{tokio_postgres, PgConnectionManager};
use std::str::FromStr;
use std::time::Duration;
use tokio_postgres::{Config, NoTls};

const DB_POOL_MAX_OPEN: u64 = 1000;
const DB_POOL_MAX_IDLE: u64 = 200;
const DB_POOL_TIMEOUT_SECONDS: u64 = 30;

pub type DBPool = Pool<PgConnectionManager<NoTls>>;

/// A database connection for running database workloads.
#[derive(Clone)]
pub struct Connection {
    pub db_pool: DBPool,
}

impl Connection {
    /// Creates a repo using default configuration for the underlying connection pool.
    pub fn new(database_url: &str) -> Self {
        Self::from_pool_builder(
            database_url,
            DB_POOL_MAX_OPEN,
            DB_POOL_MAX_IDLE,
            DB_POOL_TIMEOUT_SECONDS,
        )
    }

    /// Creates a repo with a pool builder, allowing you to customize
    /// any connection pool configuration.
    pub fn from_pool_builder(
        database_url: &str,
        max_open: u64,
        max_iddle: u64,
        timeout: u64,
    ) -> Self {
        let config = Config::from_str(&database_url).unwrap();

        let manager = PgConnectionManager::new(config, NoTls);
        let db_pool = Pool::builder()
            .max_open(max_open)
            .max_idle(max_iddle)
            .get_timeout(Some(Duration::from_secs(timeout)))
            .build(manager);

        Self { db_pool }
    }
}
