-- USERS 

CREATE TABLE users (
	id bigserial NOT NULL,
	email varchar(50) NOT NULL,
	CONSTRAINT users_pkey PRIMARY KEY (id)
);

-- Insert default users

INSERT INTO users (id, email) VALUES (1, 'abc@gmail.com');
INSERT INTO users (id, email) VALUES (2, 'abcd@gmail.com');
INSERT INTO users (id, email) VALUES (3, 'abce@gmail.com');
INSERT INTO users (id, email) VALUES (4, 'abcf@gmail.com');
INSERT INTO users (id, email) VALUES (5, 'abcg@gmail.com');
INSERT INTO users (id, email) VALUES (6, 'abch@gmail.com');
INSERT INTO users (id, email) VALUES (7, 'abci@gmail.com');

-- QUESTIONS

CREATE TABLE questions (
	id bigserial NOT NULL,
	question varchar(100) NOT NULL,
	CONSTRAINT questions_pkey PRIMARY KEY (id)
);

-- Insert default questions

INSERT INTO questions (id, question) VALUES (1, 'Which smartphone do you own?');
INSERT INTO questions (id, question) VALUES (2, 'How do you travel most regularly?');

-- ANSWERS

CREATE TABLE answers (
	id bigserial NOT NULL,
	question_id bigserial NOT NULL,
	answer varchar(100) NOT NULL,
	CONSTRAINT answers_pkey PRIMARY KEY (id)
);

-- answers foreign keys

ALTER TABLE answers ADD CONSTRAINT answers_question_id_fkey FOREIGN KEY (question_id) REFERENCES questions(id) ON DELETE CASCADE;

-- Insert default answers

INSERT INTO answers (id, question_id, answer) VALUES (1, 1, 'Android');
INSERT INTO answers (id, question_id, answer) VALUES (2, 1, 'iPhone');
INSERT INTO answers (id, question_id, answer) VALUES (3, 1, 'Windows');
INSERT INTO answers (id, question_id, answer) VALUES (4, 1, 'Other');
INSERT INTO answers (id, question_id, answer) VALUES (5, 2, 'By car');
INSERT INTO answers (id, question_id, answer) VALUES (6, 2, 'By public transport');
INSERT INTO answers (id, question_id, answer) VALUES (7, 2, 'By bicycle');
INSERT INTO answers (id, question_id, answer) VALUES (8, 2, 'Other');

-- RESPONSES

CREATE TABLE responses (
	user_id bigserial NOT NULL,
	question_id bigserial NOT NULL,
	answer_id bigserial NOT NULL,
	CONSTRAINT responses_pkey PRIMARY KEY (user_id, question_id)
);

-- responses foreign keys

ALTER TABLE responses ADD CONSTRAINT responses_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;
ALTER TABLE responses ADD CONSTRAINT responses_question_id_fkey FOREIGN KEY (question_id) REFERENCES questions(id) ON DELETE CASCADE;
ALTER TABLE responses ADD CONSTRAINT responses_answer_id_fkey FOREIGN KEY (answer_id) REFERENCES answers(id) ON DELETE CASCADE;

-- Insert default responses

INSERT INTO responses (user_id, question_id, answer_id) VALUES (1, 1, 1);
INSERT INTO responses (user_id, question_id, answer_id) VALUES (1, 2, 5);
INSERT INTO responses (user_id, question_id, answer_id) VALUES (2, 1, 2);
INSERT INTO responses (user_id, question_id, answer_id) VALUES (2, 2, 5);
INSERT INTO responses (user_id, question_id, answer_id) VALUES (3, 1, 3);
INSERT INTO responses (user_id, question_id, answer_id) VALUES (3, 2, 6);
INSERT INTO responses (user_id, question_id, answer_id) VALUES (4, 1, 2);
INSERT INTO responses (user_id, question_id, answer_id) VALUES (4, 2, 5);
INSERT INTO responses (user_id, question_id, answer_id) VALUES (5, 1, 2);
INSERT INTO responses (user_id, question_id, answer_id) VALUES (5, 2, 5);
INSERT INTO responses (user_id, question_id, answer_id) VALUES (6, 1, 1);
INSERT INTO responses (user_id, question_id, answer_id) VALUES (6, 2, 8);
INSERT INTO responses (user_id, question_id, answer_id) VALUES (7, 1, 2);
INSERT INTO responses (user_id, question_id, answer_id) VALUES (7, 2, 5);