use db::{Connection, UserRepository};
use std::sync::Arc;
use web::{
    graphql,
    server::{start_server, Config},
    services::UserService,
};

#[tokio::main]
async fn main() {
    let server_address = dotenv::var("SERVER_ADDRESS")
        .expect("SERVER_ADDRESS not present")
        .parse()
        .unwrap();

    let config = Config {
        graphql_context: create_context(),
        address: server_address,
    };

    println!("Started server at {}", config.address);
    start_server(config).await;
}

fn create_context() -> graphql::Context {
    let url = dotenv::var("DATABASE_URL").expect("DATABASE_URL not present");
    let db_con = Connection::new(&url);
    let db_con = Arc::new(db_con);

    let user_repository = UserRepository::new(db_con);

    let user_service = UserService::new(user_repository);

    graphql::Context {
        user_service,
    }
}
