use crate::graphql::{Context, Schema};
use juniper::http::GraphQLRequest;
use std::convert::Infallible;
use std::sync::Arc;

pub async fn graphql(
    schema: Arc<Schema>,
    ctx: Arc<Context>,
    req: GraphQLRequest,
) -> Result<impl warp::Reply, Infallible> {
    let res = req.execute_async(&schema, &ctx).await;
    let json = serde_json::to_string(&res).expect("Invalid JSON response");
    Ok(json)
}
