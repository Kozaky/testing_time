use crate::errors::AppError;
use crate::graphql::CountUsersArgsEnum;
use db::UserRepository;
use domain::UserRepository as UserRepositoryTrait;

pub struct UserService {
    user_repository: UserRepository,
}

impl UserService {
    pub fn new(user_repository: UserRepository) -> Self {
        Self { user_repository }
    }

    pub async fn count_users(&self, args: Vec<CountUsersArgsEnum>) -> Result<String, AppError> {
        let question_ids = args
            .iter()
            .map(|arg| match arg {
                CountUsersArgsEnum::HasIPhone => 1,
                CountUsersArgsEnum::ByCar => 2,
            })
            .collect();

        let answer_ids = args
            .iter()
            .map(|arg| match arg {
                CountUsersArgsEnum::HasIPhone => 2,
                CountUsersArgsEnum::ByCar => 5,
            })
            .collect();

        let total = self
            .user_repository
            .count_users(&question_ids, &answer_ids)
            .await?;

        Ok(total.to_string())
    }
}
