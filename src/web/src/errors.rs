use domain::error::DomainError;
use juniper::{FieldError, IntoFieldError, Value};
use std::fmt;

#[derive(Debug, Clone)]
pub enum AppErrorType {
    DomainError,
    NotFoundError,
    InvalidField,
}

pub enum InvalidFieldKind {
    NotaNumber,
}

#[derive(Debug, Clone)]
pub struct AppError {
    pub message: Option<String>,
    pub cause: Option<String>,
    pub error_type: AppErrorType,
}

impl AppError {
    pub fn new_field_err(field: &str, kind: InvalidFieldKind) -> Self {
        match kind {
            InvalidFieldKind::NotaNumber => Self {
                cause: None,
                message: Some(format!("The field: {} must be a number", field)),
                error_type: AppErrorType::InvalidField,
            },
        }
    }

    pub fn message(&self) -> String {
        match &*self {
            AppError {
                message: Some(message),
                ..
            } => message.clone(),
            AppError {
                error_type: AppErrorType::NotFoundError,
                ..
            } => "The requested item was not found".to_owned(),
            AppError {
                error_type: AppErrorType::InvalidField,
                ..
            } => "Invalid field value provided".to_owned(),
            error => {
                println!("{:?}", error);
                "An unexpected error has occurred".to_owned()
            }
        }
    }
}

impl IntoFieldError for AppError {
    fn into_field_error(self) -> FieldError {
        FieldError::new(self.message(), Value::null())
    }
}

impl From<DomainError> for AppError {
    fn from(error: DomainError) -> Self {
        AppError {
            message: None,
            cause: error.cause,
            error_type: AppErrorType::DomainError,
        }
    }
}

impl fmt::Display for AppError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
        write!(f, "{}", self.message())
    }
}
