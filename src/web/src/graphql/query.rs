use crate::errors::AppError;
use crate::graphql::{Context, type_defs::CountUsersArgsEnum};

/// Struct for GraphQL queries
pub struct QueryRoot;

#[juniper::graphql_object(Context = Context)]
impl QueryRoot {
    // Users
    async fn count_users(ctx: &Context, args: Vec<CountUsersArgsEnum>) -> Result<String, AppError> {
        ctx.user_service.count_users(args).await
    }
}
