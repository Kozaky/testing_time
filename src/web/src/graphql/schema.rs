use crate::graphql::{QueryRoot, MutationRoot};
use crate::services::UserService;
use juniper::RootNode;

pub type Schema = RootNode<'static, QueryRoot, MutationRoot>;

pub fn new() -> Schema {
    Schema::new(QueryRoot, MutationRoot)
}

/// Context shared in all GraphQL queries
pub struct Context {
    pub user_service: UserService,
}

impl juniper::Context for Context {}
