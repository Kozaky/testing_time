use crate::graphql::Context;

/// Struct for GraphQL mutations
pub struct MutationRoot;

#[juniper::graphql_object(Context = Context)]
impl MutationRoot {}
