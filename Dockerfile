FROM rust:latest as builder

RUN USER=root cargo new testing_time
RUN pwd
WORKDIR ./testing_time/src
RUN pwd
RUN rm *.rs
RUN USER=root cargo new --bin application
RUN USER=root cargo new --lib db
RUN USER=root cargo new --lib domain
RUN USER=root cargo new --lib web
WORKDIR ..
RUN pwd
COPY ./Cargo.toml ./Cargo.toml
COPY ./src/application/Cargo.toml ./src/application/Cargo.toml
COPY ./src/db/Cargo.toml ./src/db/Cargo.toml
COPY ./src/domain/Cargo.toml ./src/domain/Cargo.toml
COPY ./src/web/Cargo.toml ./src/web/Cargo.toml
RUN cargo build --release
RUN rm src/application/src/*.rs
RUN rm src/db/src/*.rs
RUN rm src/domain/src/*.rs
RUN rm src/web/src/*.rs

ADD . ./

RUN rm ./target/release/deps/testing_time*
RUN cargo build --release



FROM debian:buster-slim
ARG APP=/usr/src/app

RUN apt-get update \
    && apt-get install -y ca-certificates tzdata \
    && rm -rf /var/lib/apt/lists/*

EXPOSE 4040

ENV TZ=Etc/UTC \
    APP_USER=appuser

RUN groupadd $APP_USER \
    && useradd -g $APP_USER $APP_USER \
    && mkdir -p ${APP}

COPY --from=builder /testing_time/target/release/testing_time_application ${APP}/testing_time

RUN chown -R $APP_USER:$APP_USER ${APP}

USER $APP_USER
WORKDIR ${APP}


CMD ["./testing_time"]