# TestingTime App

This applications has been made using my own public GraphQL template: https://gitlab.com/Kozaky/graphql_template

The project uses:
- Warp, as the server
- Juniper, to implement GraphQL
- tokio-postgres, which will allow us to query a PostgreSQL database
- Refinery, to make database migrations

And it is composed of the following modules:
- domain, where the entities needed are defined 
- db defines queries and the database access
- web, which will configure a warp server for us along with some GraphQL services.
- application, where the general configuration is provided and the warp server is started.

To run the application locally, you will need to install Docker. Once you have it, you can go to the project directory and run the following command:

~~~
docker-compose up -d
~~~

This can take a while but, once it finishes, you will be able to access the website at http://localhost:4040/graphiql

Once there, you should follow this small demo:

![CountUsers Demo](demos/count_users_demo.gif)